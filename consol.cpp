#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>
using namespace std;
int max_left = -1000000000 - 9, max_right = -1000000000 - 9;
int max_version = 0;

struct NodeTree
{
	int left;
	int right;
	int value;
	int number_version;
	int last_version;
	int left_gran;
	int right_gran;
};

std::vector <NodeTree> node_tree;
std::vector <int> sqr_version;
int version;

int find(int left, int right, int curr, int curren_l, int curren_r)
{
    if (left == curren_l && right == curren_r)
	{
		return curr;
	}
	if (left <= curren_l && right >= curren_r)
	{
		return curr;
	}
	int mid = curren_l+(-curren_l + curren_r) / 2;
	if (mid >= right)
	{
		return find(left, right, node_tree[curr].left, curren_l, mid);
	}
	return find(left, right, node_tree[curr].right, mid + 1, curren_r);
}

int PushNewElement(int left, int right, int value)
{
    if (left == right)
	{
	    NodeTree newn;
		newn.left = 0;
		newn.right = 0;
		newn.left_gran = left;
		newn.right_gran = right;
		newn.number_version = version;
		newn.last_version = find(left, right, sqr_version[version], max_left, max_right);
		newn.value = node_tree[newn.last_version].value + 1;
		node_tree.push_back(newn);
		return node_tree.size() - 1;
	}
	int mid = left+(right-left)/2;
	if (value <= mid)
	{
		NodeTree newn;
		int curr_n = node_tree.size();
		newn.left_gran = left;
		newn.right_gran = right;
		newn.right = node_tree[find(left, right, sqr_version[version], max_left, max_right)].right;
		newn.left = 0;
		newn.number_version = version;
		newn.last_version = find(left, right, sqr_version[version], max_left, max_right);
		node_tree.push_back(newn);
		int lol = PushNewElement(left, mid, value);
        node_tree[curr_n].left = lol;
        node_tree[curr_n].value = node_tree[node_tree[curr_n].left].value + node_tree[node_tree[curr_n].right].value;
        return curr_n;
	}
	NodeTree newn;
	newn.left_gran = left;
	newn.right_gran = right;
	int curr_n = node_tree.size();
	newn.left = node_tree[find(left, right, sqr_version[version], max_left, max_right)].left;
	newn.right = 0;
	newn.number_version = version;
	newn.last_version = find(left, right, sqr_version[version], max_left, max_right);
	node_tree.push_back(newn);
	int lol = PushNewElement(mid + 1, right, value);
	node_tree[curr_n].right = lol;
	node_tree[curr_n].value = node_tree[node_tree[curr_n].left].value + node_tree[node_tree[curr_n].right].value;
    return curr_n;
}
int  nou_l;
int nou_r;
int sum(int left, int right, int curr)
{

    if (left >= nou_l && right <= nou_r)
	{
		return node_tree[curr].value;
	}
	if (node_tree[curr].value == 0)
    {
        return  0;
    }
	if (left == right)
	{
		return 0;
	}
	int mid = left+(right-left)/2;
	return sum(left, mid, node_tree[curr].left) + sum(mid + 1, right, node_tree[curr].right);
}

using std::cin;

int main()
{
    string name_file;
    std::cout << "input file name" << std::endl;
    cin >> name_file;
    ifstream ffin (name_file);
    if (ffin.is_open() == false)
    {
        std::cout << "Wrong file name. Input from keyboard." << std::endl;
    }
    string m_l, m_r;
    ofstream fout ("output.txt");
    if (ffin.is_open() == false)
    {
    cin >> m_l >> m_r;
    }
    else{
       ffin >> m_l >> m_r;
    }
    max_left = atoi(m_l.c_str());
    max_right = atoi(m_r.c_str());
    //ofstream cout ("output.txt");
    NodeTree fir;
    fir.left_gran = max_left;
    fir.right_gran = max_right;
    fir.last_version = 0;
    fir.left = 0;
    fir.right = 0;
    fir.value = 0;
    fir.number_version = 0;
    node_tree.push_back(fir);
    node_tree.push_back(fir);
    sqr_version.push_back(0);
	if (max_right < max_left)
	{
		swap(max_left, max_right);
	}
	string param;
	if (ffin.is_open() == false)
    {
	while (cin >> param)
	{

		if (param == "1")
		{
			int value;
			cin >> m_l >> m_r;
			version = atoi (m_l.c_str());
			value = atoi (m_r.c_str());
			if (version < 0 || version > max_version)
			{
			    version = max_version;
			}
            if (value < max_left || value > max_right)
            {
                value = max_left;
            }
            max_version++;
			sqr_version.push_back(PushNewElement(max_left, max_right, value));

			cout << sqr_version.size() - 1 << endl;
		}
		else
		{
			int left, right;
			cin >> m_l;
			version = atoi (m_l.c_str());
            cin >> m_l >> m_r;
            left = atoi (m_l.c_str());
            right = atoi (m_r.c_str());

			if (version < 0 || version > max_version)
			{
			    version = max_version;
			}
            if (left < max_left || left > max_right)
            {
                left = max_left;
            }
            if (right < max_left || right > max_right)
            {
                right = max_right;
            }
            if (left > right)
            {
                swap(left, right);
            }
            nou_l = left;
            nou_r = right;
			cout << sum(max_left, max_right, sqr_version[version])<<endl;
		}
	}
	return 0;
    }
    while (ffin >> param)
	{

		if (param == "1")
		{
			int value;
			ffin >> m_l >> m_r;
			version = atoi (m_l.c_str());
			value = atoi (m_r.c_str());
			if (version < 0 || version > max_version)
			{
			    version = max_version;
			}
            if (value < max_left || value > max_right)
            {
                value = max_left;
            }
            max_version++;
			sqr_version.push_back(PushNewElement(max_left, max_right, value));

			fout << sqr_version.size() - 1 << endl;
		}
		else
		{
			int left, right;
			ffin >> m_l;
			version = atoi (m_l.c_str());
            ffin >> m_l >> m_r;
            left = atoi (m_l.c_str());
            right = atoi (m_r.c_str());

			if (version < 0 || version > max_version)
			{
			    version = max_version;
			}
            if (left < max_left || left > max_right)
            {
                left = max_left;
            }
            if (right < max_left || right > max_right)
            {
                right = max_right;
            }
            if (left > right)
            {
                swap(left, right);
            }
            nou_l = left;
            nou_r = right;
			fout << sum(max_left, max_right, sqr_version[version])<<endl;
		}
	}
}
