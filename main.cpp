//Create by anton24. Winapi isn't too good but...
//W_Y_K
//You Realy want to look this? Good luck...
#define _CRT_SECURE_NO_WARNINGS


//from here

#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
//to here

#include <windows.h> // подключение библиотеки с функциями API
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <cmath>

#define RADBUTFILE 1001
#define RADBUTCONS 1002
#define PUSHBUT 1003
#define PUSHBUT2 1004
#define RADBUTCONSOL 2005
#define RADBUTDRAW 2006
#define INPUTBO 5002
#define LEFTGRAN 5003
#define BUFFSIZE 512



#include <sstream>

namespace patch
{
	template < typename T > std::string to_string(const T& n)
	{
		std::ostringstream stm;
		stm << n;
		return stm.str();
	}
}

// Глобальные переменные:
HINSTANCE hInst; 	// Указатель приложения
LPCTSTR szWindowClass = "QWERTY";
LPCTSTR szTitle = "File Reader";

HWND hRadButTxt, hRadButBin, hPushBut, hOut, hPushButApply, hRadButGraf, hRadButConsol, right_but, left_but, ok_but;
HWND edit_win, edit_win_r, edit_win_l, edit_win_dra, add_but, edit_win_r_1, edit_win_l_1, edit_win_dra_1;
HDC hdc;
int curr_top_button = 0;
int number_of_flag = 0;
BOOL Line(int x1, int y1, int x2, int y2)
{
	HPEN hPen; //Объявляется кисть
	hPen = CreatePen(PS_SOLID, 3, RGB(0, 0, 0)); //Создаётся объект
	SelectObject(hdc, hPen); //Объект делается текущим
	MoveToEx(hdc, x1, y1, NULL); //сделать текущими координаты x1, y1
	LineTo(hdc, x2, y2);
	return true;
}

OPENFILENAME ofn;
char FileBuff[128];
HANDLE hFile;

char ReadFileBuff[BUFFSIZE];

bool isTxtOrBin = true;

OVERLAPPED ol = { 0 };
int max_left = -1000000000 - 9, max_right = -1000000000 - 9;
int max_version = 0;
// Предварительное описание функций
ATOM MyRegisterClass(HINSTANCE hInstance);
BOOL InitInstance(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void MakeScreen(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
// Основная программа
int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	MSG msg;

	// Регистрация класса окна
	MyRegisterClass(hInstance);

	// Создание окна приложения
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}
	// Цикл обработки сообщений
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}


//  FUNCTION: MyRegisterClass()
//  Регистрирует класс окна
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;	// стиль окна
	wcex.lpfnWndProc = (WNDPROC)WndProc; // оконная процедура
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;	// указатель приложения
	wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION); // определение иконки
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW); // определение курсора
	wcex.hbrBackground = GetSysColorBrush(COLOR_WINDOW);   // уста-новка фона
	wcex.lpszMenuName = NULL;		// определение меню
	wcex.lpszClassName = szWindowClass;	// имя класса
	wcex.hIconSm = NULL;

	return RegisterClassEx(&wcex); // регистрация класса окна
}


// FUNCTION: InitInstance(HANDLE, int)
// Создает окно приложения и сохраняет указатель приложения в переменной hInst
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // сохраняет указатель приложения в переменной hInst

	hWnd = CreateWindow(szWindowClass, // имя класса окна
		szTitle,   // имя приложения
		WS_OVERLAPPEDWINDOW, // стиль окна
		CW_USEDEFAULT,	// положение по Х
		CW_USEDEFAULT, 	// положение по Y
		CW_USEDEFAULT,    // размер по Х
		CW_USEDEFAULT,    // размер по Y
		NULL,	// описатель родительского окна
		NULL,       // описатель меню окна
		hInstance,  // указатель приложения
		NULL);     // параметры создания.

	if (!hWnd) // Если окно не создалось, функция возвращает FALSE
	{
		return FALSE;
	}
	ShowWindow(hWnd, nCmdShow);		// Показать окно
	UpdateWindow(hWnd);			// Обновить окно
	return TRUE;				//Успешное завершение функции
}

int flag = -1;
int sup_flag = 0;
int sup_2 = 0;
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//  Оконная процедура. Принимает и обрабатывает все сообщения, приходящие в приложение

struct NodeTree
{
	int left;
	int right;
	int value;
	int number_version;
	int last_version;
	int left_gran;
	int right_gran;
};
HWND buttons_node[40];
std::vector <NodeTree> node_tree;
std::vector <int> sqr_version;
int version;
int curr_l;
int curr_r;
RECT rtt;
int parentx[40];
int parenty[40];
void DrawElement(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam, int num_b, int num_in_tree, int x, int y, int left, int right)
{
	number_of_flag = 0;
	if (left > right)
	{
		return;
	}
	if (x > 4)
	{
		return;
	}
	if (num_in_tree == 0)
	{
		return;
	}

	GetClientRect(hWnd, &rtt);
	int high = 190;
	int bot = rtt.bottom;

	if (version - 1 == node_tree[num_in_tree].number_version)
	{
		std::string value_but;
		std::string value_but2;
		int x_koor = int(rtt.right) / int((pow(2, x - 1)) + 1);
		if (x == 4)
		{
			y--;
			x_koor = int(rtt.right) / int((pow(2, x - 1)));
		}
		int kef = 0;
		if (x != 4)
		{
			kef = 80;
		}
		int y_koor = (bot - high) / 4;
		value_but = patch::to_string(x_koor) + " " + patch::to_string(y_koor);
		value_but2 = patch::to_string(x_koor) + " " + patch::to_string(y_koor);
		value_but = "Left=" +
			patch::to_string(node_tree[num_in_tree].left_gran) + '\n' +
			"Right=" +
			patch::to_string(node_tree[num_in_tree].right_gran) + '\n' +
			"Value=" + patch::to_string(node_tree[node_tree[num_in_tree].last_version].value);
		value_but2 = "Left=" +
			patch::to_string(node_tree[num_in_tree].left_gran) + '\n' +
			"Right=" +
			patch::to_string(node_tree[num_in_tree].right_gran) + '\n' +
			"Value=" + patch::to_string(node_tree[num_in_tree].value);
		if (left == max_left && right == max_right)
		{
			parentx[0] = x_koor * y - kef + 80;
			parenty[0] = high + y_koor * (x - 1) - 30;
		}
		else
		{
			if (num_b == 1)
			{
				parentx[0] = x_koor * y - kef + 80;
				parenty[0] = 0;
			}
		}
		parentx[num_b] = x_koor * y - kef + 80;
		parenty[num_b] = high + y_koor * (x - 1) - 30;
		hdc = GetDC(hWnd);
		Line(parentx[num_b], parenty[num_b], parentx[num_b / 2], parenty[num_b / 2]);
		ReleaseDC(hWnd, hdc);
		buttons_node[num_b] = CreateWindow(TEXT("button"), value_but.c_str(),
			WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_MULTILINE,
			x_koor * y - kef, high + y_koor * (x - 1) - 30,

			80, 60,
			hWnd,
			(HMENU)(3000 + num_b),
			hInst,
			0);
		buttons_node[num_b + 16] = CreateWindow(TEXT("button"), value_but2.c_str(),
			WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_MULTILINE,
			x_koor * y + 80 - kef, high + y_koor * (x - 1) - 30,

			80, 60,
			hWnd,
			(HMENU)(3000 + num_b + 16),
			hInst,
			0);
		int mid = (left + right) / 2;
		DrawElement(hWnd, message, wParam, lParam, num_b * 2, node_tree[num_in_tree].left, x + 1, y * 2 - 1, left, mid);
		DrawElement(hWnd, message, wParam, lParam, num_b * 2 + 1, node_tree[num_in_tree].right, x + 1, y * 2, mid + 1, right);

		return;
	}
	std::string value_but;
	//char* value;
	//	char buff[20];

	value_but = "Left = " +
		patch::to_string(node_tree[num_in_tree].left_gran) + '\n' +
		"Right=" +
		patch::to_string(node_tree[num_in_tree].right_gran) + '\n' +
		"Value=" + patch::to_string(node_tree[num_in_tree].value);


	int x_koor = int(rtt.right) / int((pow(2, x - 1)) + 1);
	if (x == 4)
	{
		y--;
		x_koor = int(rtt.right) / int((pow(2, x - 1)));
	}
	int kef = 0;
	if (x != 4)
	{
		kef = 80;
	}
	int y_koor = (bot - high) / 4;
	if (left == max_left && right == max_right)
	{
		parentx[0] = x_koor * y - kef + 40;
		parenty[0] = high + y_koor * (x - 1) - 30;
	}
	else
	{
		if (num_b == 1)
		{
			parentx[0] = x_koor * y - kef + 40;
			parenty[0] = 0;
		}
	}
	parentx[num_b] = x_koor * y - kef + 40;
	parenty[num_b] = high + y_koor * (x - 1) - 30;
	hdc = GetDC(hWnd);
	Line(parentx[num_b], parenty[num_b], parentx[num_b / 2], parenty[num_b / 2]);
	ReleaseDC(hWnd, hdc);

	buttons_node[num_b] = CreateWindow("button", value_but.c_str(),
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_MULTILINE,
		x_koor * y - kef, high + y_koor * (x - 1) - 30, 80, 60,
		hWnd,
		(HMENU)(3000 + num_b),
		hInst,
		0);
	int mid = (left + right) / 2;
	DrawElement(hWnd, message, wParam, lParam, num_b * 2, node_tree[num_in_tree].left, x + 1, y * 2 - 1, left, mid);
	DrawElement(hWnd, message, wParam, lParam, num_b * 2 + 1, node_tree[num_in_tree].right, x + 1, y * 2, mid + 1, right);
}
int numbers_node_tree;

int find(int left, int right, int curr, int curren_l, int curren_r)
{
	if (left == curren_l && right == curren_r)
	{
		return curr;
	}
	if (left <= curren_l && right >= curren_r)
	{
		return curr;
	}
	int mid = (curren_l + curren_r) / 2;
	if (mid >= right)
	{
		return find(left, right, node_tree[curr].left, curren_l, mid);
	}
	return find(left, right, node_tree[curr].right, mid + 1, curren_r);
}

void DrawElement2(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam, int num_b, int num_in_tree, int x, int y, int left, int right, int need_l, int need_r, bool was_ch)
{
	number_of_flag = 1;
	if (left > right)
	{
		return;
	}
	if (x > 4)
	{
		return;
	}


	GetClientRect(hWnd, &rtt);
	int high = 190;
	int bot = rtt.bottom;


	std::string value_but;
	value_but = "Left=" +
		patch::to_string(left) + '\n' +
		"Right=" +
		patch::to_string(right) + '\n' +
		"Value=" + patch::to_string(node_tree[num_in_tree].value);


	int x_koor = int(rtt.right) / int((pow(2, x - 1)) + 1);
	if (x == 4)
	{
		y--;
		x_koor = int(rtt.right) / int((pow(2, x - 1)));
	}
	int kef = 0;
	if (x != 4)
	{
		kef = 80;
	}
	int y_koor = (bot - high) / 4;
	if (left == max_left && right == max_right)
	{
		parentx[0] = x_koor * y - kef + 40;
		parenty[0] = high + y_koor * (x - 1) - 30;
	}
	else
	{
		if (num_b == 1)
		{
			parentx[0] = x_koor * y - kef + 40;
			parenty[0] = 0;
		}
	}
	parentx[num_b] = x_koor * y - kef + 40;
	parenty[num_b] = high + y_koor * (x - 1) - 30;
	hdc = GetDC(hWnd);
	Line(parentx[num_b], parenty[num_b], parentx[num_b / 2], parenty[num_b / 2]);
	ReleaseDC(hWnd, hdc);
	if (need_l <= left && need_r >= right && was_ch == false)
	{
		hdc = GetDC(hWnd);
		HPEN hPen;
		hPen = CreatePen(PS_SOLID, 3, RGB(255, 0, 0));
		SelectObject(hdc, hPen);

		Ellipse(hdc, x_koor * y - kef - 10, high + y_koor * (x - 1) - 30 + 80, x_koor * y - kef + 100, high + y_koor * (x - 1) - 30 - 10);
		ReleaseDC(hWnd, hdc);
		was_ch = true;

	}
	int id = 3000 + num_b;
	if (right != node_tree[num_in_tree].right_gran &&	left != node_tree[num_in_tree].left_gran)
	{
		id += 16;
	}
	buttons_node[num_b] = CreateWindow("button", value_but.c_str(),
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_MULTILINE,
		x_koor * y - kef, high + y_koor * (x - 1) - 30, 80, 60,
		hWnd,
		(HMENU)id,
		hInst,
		0);
	if (left == right)
	{
		return;
	}
	int mid = (left + right) / 2;
	DrawElement2(hWnd, message, wParam, lParam, num_b * 2, node_tree[num_in_tree].left, x + 1, y * 2 - 1, left, mid, need_l, need_r, was_ch);
	DrawElement2(hWnd, message, wParam, lParam, num_b * 2 + 1, node_tree[num_in_tree].right, x + 1, y * 2, mid + 1, right, need_l, need_r, was_ch);
}

int last_size = -1;
std::vector <int> zero_last;

int PushNewElement(int left, int right, int value)
{
	int need = 0;
	if (last_size == right - left)
	{
		NodeTree newn;
		newn.left = 0;
		newn.right = 0;
		newn.left_gran = 0;
		newn.right_gran = 0;
		newn.number_version = version;
		newn.last_version = 0;
		newn.value = 1;
		node_tree.push_back(newn);
		//return 0;
		return node_tree.size() - 1;
	}
	last_size = right - left;
	if (left == right)
	{
		NodeTree newn;
		newn.left = 0;
		newn.right = 0;
		newn.left_gran = left;
		newn.right_gran = right;
		newn.number_version = version;
		newn.last_version = find(left, right, sqr_version[version], max_left, max_right);
		newn.value = node_tree[newn.last_version].value + 1;
		node_tree.push_back(newn);
		return node_tree.size() - 1;
	}
	int mid = (left + right) / 2;
	NodeTree newn;
	int curr_n = node_tree.size();
	int lol = -100;
	if (value <= mid)
	{
		newn.left_gran = left;
		newn.right_gran = right;
		newn.right = node_tree[find(left, right, sqr_version[version], max_left, max_right)].right;
		newn.number_version = version;
		newn.last_version = find(left, right, sqr_version[version], max_left, max_right);
		if (newn.last_version == 0)
		{
			newn.right = 0;
		}
		node_tree.push_back(newn);
		node_tree[curr_n].left = PushNewElement(left, mid, value);
		lol = node_tree[curr_n].left;
		node_tree[curr_n].value = node_tree[node_tree[curr_n].left].value + node_tree[node_tree[curr_n].right].value;
	}
	else
	{
		NodeTree newn;
		newn.left_gran = left;
		newn.right_gran = right;
		newn.left = node_tree[find(left, right, sqr_version[version], max_left, max_right)].left;
		newn.number_version = version;
		newn.last_version = find(left, right, sqr_version[version], max_left, max_right);
		newn.value = 0;
		if (newn.last_version == 0)
		{
			newn.left = 0;
		}
		node_tree.push_back(newn);
		node_tree[curr_n].right = PushNewElement(mid + 1, right, value);
		lol = node_tree[curr_n].right;
		node_tree[curr_n].value = node_tree[node_tree[curr_n].left].value + node_tree[node_tree[curr_n].right].value;
	}
	return curr_n;
}

void DrawAll(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	for (int i = 0; i < 40; ++i)
	{
		DestroyWindow(buttons_node[i]);
	}

}


void MakeStartWind(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	hRadButConsol = CreateWindow("button", "Console",
		WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
		10, 10,
		120, 15,
		hWnd,
		(HMENU)RADBUTCONSOL,
		hInst,
		0);

	hRadButGraf = CreateWindow("button", "Draw",
		WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
		10, 30,
		120, 15,
		hWnd,
		(HMENU)RADBUTDRAW,
		hInst,
		0);

	hPushBut = CreateWindow("button", "Choose file",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		10, 70,
		150, 50,
		hWnd,
		(HMENU)PUSHBUT,
		hInst,
		0);

	hPushButApply = CreateWindow("button", "Apply",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		20, 200,
		100, 100,
		hWnd,
		(HMENU)PUSHBUT2,
		hInst,
		0);
}

void InputBorder(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	max_left = 0;
	max_right = 100;
	ok_but = CreateWindow("button", "ok",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		40, 70,
		50, 50,
		hWnd,
		(HMENU)INPUTBO,
		hInst,
		0);
	edit_win_l = CreateWindow(TEXT("EDIT"), TEXT("input left"),
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		20, 30,
		100, 20,
		hWnd,
		(HMENU)LEFTGRAN,
		hInst,
		0);
	edit_win_r = CreateWindow(TEXT("EDIT"), TEXT("input right"),
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		20, 50,
		100, 20,
		hWnd,
		(HMENU)5004,
		hInst,
		0);

}
void MakeScreen(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	left_but = CreateWindow("button", "draw vers",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		20, 90,
		80, 80,
		hWnd,
		(HMENU)5000,
		hInst,
		0);
	add_but = CreateWindow("button", "add element",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		200, 90,
		100, 80,
		hWnd,
		(HMENU)5006,
		hInst,
		0);
	right_but = CreateWindow("button", "draw sum",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		110, 90,
		80, 80,
		hWnd,
		(HMENU)5001,
		hInst,
		0);
	edit_win_l = CreateWindow(TEXT("EDIT"), TEXT("input vers"),
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		20, 30,
		80, 20,
		hWnd,
		(HMENU)8001,
		hInst,
		0);
	edit_win_r = CreateWindow(TEXT("EDIT"), TEXT("input vers"),
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		200, 30,
		80, 20,
		hWnd,
		(HMENU)8002,
		hInst,
		0);
	edit_win_dra = CreateWindow(TEXT("EDIT"), TEXT("input value"),
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		200, 50,
		80, 20,
		hWnd,
		(HMENU)8003,
		hInst,
		0);
	edit_win_l_1 = CreateWindow(TEXT("EDIT"), TEXT("input left"),
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		110, 70,
		80, 20,
		hWnd,
		(HMENU)8004,
		hInst,
		0);
	edit_win_r_1 = CreateWindow(TEXT("EDIT"), TEXT("input right"),
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		110, 50,
		80, 20,
		hWnd,
		(HMENU)8005,
		hInst,
		0);
	edit_win_dra_1 = CreateWindow(TEXT("EDIT"), TEXT("input vers"),
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		110, 30,
		80, 20,
		hWnd,
		(HMENU)8006,
		hInst,
		0);

}

void Continue_work(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (number_of_flag == 0)
	{
		DestroyWindow(ok_but);
		DestroyWindow(edit_win_r);
		DestroyWindow(edit_win_l);
		DestroyWindow(edit_win_dra);
		DestroyWindow(edit_win_r_1);
		DestroyWindow(edit_win_l_1);
		DestroyWindow(edit_win_dra_1);
		DrawAll(hWnd, message, wParam, lParam);
		hdc = GetDC(hWnd);
		RECT rectti;
		GetClientRect(hWnd, &rectti);
		FillRect(hdc, &rectti, (HBRUSH)(COLOR_WINDOW + 1));
		ReleaseDC(hWnd, hdc);
		DrawElement(hWnd, message, wParam, lParam, 1, curr_top_button, 1, 1, node_tree[curr_top_button].left_gran, node_tree[curr_top_button].right_gran);
		MakeScreen(hWnd, message, wParam, lParam);
	}
	else
	{
		DestroyWindow(ok_but);
		DestroyWindow(edit_win_r);
		DestroyWindow(edit_win_l);
		DestroyWindow(edit_win_dra);
		DestroyWindow(edit_win_r_1);
		DestroyWindow(edit_win_l_1);
		DestroyWindow(edit_win_dra_1);
		DrawAll(hWnd, message, wParam, lParam);
		hdc = GetDC(hWnd);
		RECT rectti;
		GetClientRect(hWnd, &rectti);
		FillRect(hdc, &rectti, (HBRUSH)(COLOR_WINDOW + 1));
		ReleaseDC(hWnd, hdc);
		DrawElement2(hWnd, message, wParam, lParam, 1, curr_top_button, 1, 1, node_tree[curr_top_button].left_gran, node_tree[curr_top_button].right_gran, curr_l, curr_r, false);
		MakeScreen(hWnd, message, wParam, lParam);
	}
}

int find_parent(int curr_top_button, int start_plase)
{
	if (node_tree[node_tree[start_plase].left].left_gran == node_tree[curr_top_button].left_gran &&
		node_tree[node_tree[start_plase].left].right_gran == node_tree[curr_top_button].right_gran)
	{
		return start_plase;
	}
	if (node_tree[node_tree[start_plase].right].left_gran == node_tree[curr_top_button].left_gran &&
		node_tree[node_tree[start_plase].right].right_gran == node_tree[curr_top_button].right_gran)
	{
		return start_plase;
	}
	if (node_tree[start_plase].left_gran == node_tree[start_plase].right_gran)
	{
		return -1;
	}
	int mid = (node_tree[start_plase].left_gran + node_tree[start_plase].right_gran) / 2;

	return max(find_parent(curr_top_button, node_tree[start_plase].left),
		find_parent(curr_top_button, node_tree[start_plase].right));

}

void UpdateStartWindow(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD(wParam))
	{
	case RADBUTFILE:
		sup_flag = 1;
		break;

	case RADBUTCONS:
		sup_flag = 0;
		break;

	case RADBUTCONSOL:
		sup_2 = 1;
		break;

	case RADBUTDRAW:
		sup_2 = 0;
		break;

	case PUSHBUT2:
		if (sup_2 == 1)
		{
			STARTUPINFO si;
			PROCESS_INFORMATION pi;
			ZeroMemory(&si, sizeof(STARTUPINFO));
			CreateProcess(NULL, TEXT("consol.exe"), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
			PostQuitMessage(0);
		}
		flag = sup_flag;
		if (hFile == INVALID_HANDLE_VALUE && flag == 1)
		{
			flag = 0;
		}
		DestroyWindow(hRadButTxt);
		DestroyWindow(hRadButBin);
		DestroyWindow(hRadButGraf);
		DestroyWindow(hRadButConsol);
		DestroyWindow(hPushBut);
		DestroyWindow(hPushButApply);
		InputBorder(hWnd, message, wParam, lParam);
		InvalidateRect(hWnd, 0, true);
		break;

	case PUSHBUT:
		memset(ReadFileBuff, 0, BUFFSIZE);

		if (GetOpenFileName(&ofn) == TRUE)
		{
			std::ifstream fin(ofn.lpstrFile);
			fin >> max_left >> max_right;
			int value = 0;
			while (fin >> version >> value)
			{
				if (version < 0 || version > max_version)
				{
					version = max_version;
				}
				if (value < max_left || value > max_right)
				{
					value = max_left;
				}
				max_version++;
				sqr_version.push_back(PushNewElement(max_left, max_right, value));
			}
			DestroyWindow(hRadButTxt);
			DestroyWindow(hRadButBin);
			DestroyWindow(hRadButGraf);
			DestroyWindow(hRadButConsol);
			DestroyWindow(hPushBut);
			DestroyWindow(hPushButApply);
			MakeScreen(hWnd, message, wParam, lParam);

		}
		break;
	case INPUTBO:
		//input from edit
		char buffleft[10], buff2[10];
		GetWindowText(GetDlgItem(hWnd, LEFTGRAN), buffleft, 10);
		max_left = atoi(buffleft);
		GetWindowText(GetDlgItem(hWnd, 5004), buff2, 10);
		max_right = atoi(buff2);
		DestroyWindow(ok_but);
		DestroyWindow(edit_win_r);
		DestroyWindow(edit_win_l);
		DestroyWindow(edit_win_r_1);
		DestroyWindow(edit_win_l_1);
		DestroyWindow(edit_win_dra_1);
		MakeScreen(hWnd, message, wParam, lParam);
		InvalidateRect(hWnd, 0, true);
		break;
	case 5001:
		//input from edit
		char buff1[10], buff01[10], buff001[10];
		GetWindowText(GetDlgItem(hWnd, 8004), buff1, 10);
		curr_l = atoi(buff1);
		GetWindowText(GetDlgItem(hWnd, 8005), buff01, 10);
		curr_r = atoi(buff01);
		if (curr_l < max_left || curr_l > max_right || curr_r < max_left || curr_r > max_right)
		{
			curr_l = max_left;
			curr_r = max_right;
		}
		if (curr_l > curr_r)
		{
			std::swap(curr_l, curr_r);
		}
		GetWindowText(GetDlgItem(hWnd, 8006), buff001, 10);
		version = atoi(buff001);
		if (version < 0 || version > max_version)
		{
			version = max_version;
		}
		DestroyWindow(ok_but);
		DestroyWindow(edit_win_r);
		DestroyWindow(edit_win_dra);
		DestroyWindow(edit_win_l);
		DestroyWindow(edit_win_r_1);
		DestroyWindow(edit_win_l_1);
		DestroyWindow(edit_win_dra_1);
		DrawAll(hWnd, message, wParam, lParam);
		hdc = GetDC(hWnd);
		RECT rectt;
		GetClientRect(hWnd, &rectt);
		FillRect(hdc, &rectt, (HBRUSH)(COLOR_WINDOW + 1));
		ReleaseDC(hWnd, hdc);
		curr_top_button = sqr_version[version];
		DrawElement2(hWnd, message, wParam, lParam, 1, curr_top_button, 1, 1, max_left, max_right, curr_l, curr_r, false);
		MakeScreen(hWnd, message, wParam, lParam);
		break;
	case 5000:

		//delete over
		char buff[10];
		GetWindowText(GetDlgItem(hWnd, 8001), buff, 10);
		version = atoi(buff);
		if (version < 0 || version > max_version)
		{
			version = max_version;
		}
		DestroyWindow(ok_but);
		DestroyWindow(edit_win_r);
		DestroyWindow(edit_win_l);
		DestroyWindow(edit_win_dra);
		DestroyWindow(edit_win_r_1);
		DestroyWindow(edit_win_l_1);
		DestroyWindow(edit_win_dra_1);
		DrawAll(hWnd, message, wParam, lParam);
		hdc = GetDC(hWnd);
		RECT rectti;
		GetClientRect(hWnd, &rectti);
		FillRect(hdc, &rectti, (HBRUSH)(COLOR_WINDOW + 1));
		ReleaseDC(hWnd, hdc);

		curr_top_button = sqr_version[version];
		DrawElement(hWnd, message, wParam, lParam, 1, curr_top_button, 1, 1, max_left, max_right);

		MakeScreen(hWnd, message, wParam, lParam);
		break;
	case 5006:
		char bufferrr[10];
		GetWindowText(GetDlgItem(hWnd, 8002), bufferrr, 10);
		version = atoi(bufferrr);
		if (version < 0 || version > max_version)
		{
			version = max_version;
		}
		max_version++;
		int value;
		GetWindowText(GetDlgItem(hWnd, 8003), bufferrr, 10);
		value = atoi(bufferrr);
		sqr_version.push_back(PushNewElement(max_left, max_right, value));
		DestroyWindow(ok_but);
		DestroyWindow(edit_win_r);
		DestroyWindow(edit_win_l);
		DestroyWindow(edit_win_dra);
		DestroyWindow(edit_win_r_1);
		DestroyWindow(edit_win_l_1);
		DestroyWindow(edit_win_dra_1);
		DrawAll(hWnd, message, wParam, lParam);
		hdc = GetDC(hWnd);
		RECT rectto;
		GetClientRect(hWnd, &rectto);
		FillRect(hdc, &rectto, (HBRUSH)(COLOR_WINDOW + 1));
		ReleaseDC(hWnd, hdc);
		version = sqr_version.size() - 1;
		curr_top_button = sqr_version[version];

		DrawElement(hWnd, message, wParam, lParam, 1, curr_top_button, 1, 1, max_left, max_right);
		MakeScreen(hWnd, message, wParam, lParam);
		break;
	case 3002:
		curr_top_button = node_tree[curr_top_button].left;
		Continue_work(hWnd, message, wParam, lParam);
		break;
	case 3003:
		curr_top_button = node_tree[curr_top_button].right;
		Continue_work(hWnd, message, wParam, lParam);
		break;
	case 3004:
		curr_top_button = node_tree[node_tree[curr_top_button].left].left;
		Continue_work(hWnd, message, wParam, lParam);
		break;
	case 3005:
		curr_top_button = node_tree[node_tree[curr_top_button].left].right;
		Continue_work(hWnd, message, wParam, lParam);
		break;
	case 3006:
		curr_top_button = node_tree[node_tree[curr_top_button].right].left;
		Continue_work(hWnd, message, wParam, lParam);
		break;
	case 3007:
		curr_top_button = node_tree[node_tree[curr_top_button].right].right;
		Continue_work(hWnd, message, wParam, lParam);
		break;
	case 3008:
		curr_top_button = node_tree[node_tree[node_tree[curr_top_button].left].left].left;
		Continue_work(hWnd, message, wParam, lParam);
		break;
	case 3009:
		curr_top_button = node_tree[node_tree[node_tree[curr_top_button].left].left].right;
		Continue_work(hWnd, message, wParam, lParam);
		break;
	case 3010:
		curr_top_button = node_tree[node_tree[node_tree[curr_top_button].left].right].left;
		Continue_work(hWnd, message, wParam, lParam);
		break;
	case 3011:
		curr_top_button = node_tree[node_tree[node_tree[curr_top_button].left].right].right;
		Continue_work(hWnd, message, wParam, lParam);
		break;
	case 3012:
		curr_top_button = node_tree[node_tree[node_tree[curr_top_button].right].left].left;
		Continue_work(hWnd, message, wParam, lParam);
		break;
	case 3013:
		curr_top_button = node_tree[node_tree[node_tree[curr_top_button].right].left].right;
		Continue_work(hWnd, message, wParam, lParam);
		break;
	case 3014:
		curr_top_button = node_tree[node_tree[node_tree[curr_top_button].right].right].left;
		Continue_work(hWnd, message, wParam, lParam);
		break;
	case 3015:
		curr_top_button = node_tree[node_tree[node_tree[curr_top_button].right].right].right;
		Continue_work(hWnd, message, wParam, lParam);
		break;
	case 3001:
		curr_top_button = find_parent(curr_top_button, sqr_version[version]);
		if (curr_top_button == -1)
		{
			curr_top_button = sqr_version[version];
		}
		Continue_work(hWnd, message, wParam, lParam);
		break;
	}
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	//HDC hdc;
	RECT rt;

	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.lpstrFile = FileBuff;
	ofn.nMaxFile = sizeof(FileBuff);
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	switch (message)
	{
	case WM_CREATE: // Сообщение приходит при создании окна

		MakeStartWind(hWnd, message, wParam, lParam);
		zero_last.reserve(100000);
		NodeTree fir;
		fir.left_gran = max_left;
		fir.right_gran = max_right;
		fir.last_version = 0;
		fir.left = 0;
		fir.right = 0;
		fir.value = 0;
		fir.number_version = 0;
		node_tree.push_back(fir);
		node_tree.push_back(fir);
		sqr_version.push_back(0);
		break;

	case WM_PAINT: // Перерисовать окно
		hdc = BeginPaint(hWnd, &ps); // Начать графический вывод
		GetClientRect(hWnd, &rt); // Область окна для рисования
		EndPaint(hWnd, &ps); // Закончить графический вывод
		break;

	case WM_COMMAND:

		UpdateStartWindow(hWnd, message, wParam, lParam);
		break;
		break;

	case WM_DESTROY: // Завершение работы
		PostQuitMessage(0);
		break;

	default: // Обработка сообщений, которые не обработаны пользователем
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
